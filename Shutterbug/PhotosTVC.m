//
//  LatestFlickrPhotosTVCViewController.m
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "PhotosTVC.h"
#import "FlickrFetcher.h"
#import "PhotoViewController.h"

@interface PhotosTVC ()

@end

@implementation PhotosTVC

#define SPOT_TITLE @"spot_title"
#define SPOT_DESCRIPTION @"spot_description"
#define SPOT_ID @"spot_id"
#define SPOT_LARGE_PHOTO @"spot_large_photo"
#define SPOT_ORIGINAL_PHOTO @"spot_original_photo"

-(NSArray *)listOfPhotos {
    if (!_listOfPhotos) _listOfPhotos = [[NSArray alloc] init];
    return _listOfPhotos;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.refreshControl addTarget:self action:@selector(getLatestPhotos) forControlEvents:UIControlEventValueChanged];
}

-(void)getLatestPhotos {
    [self.refreshControl beginRefreshing];
    dispatch_queue_t photosQueue = dispatch_queue_create("photos queue", NULL);
    dispatch_async(photosQueue, ^{
        // With the following method we reload? the photos, but do we need to do something in the main thread?
        [self listOfPhotos];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
        });
    });
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[PhotoViewController class]]) {
        PhotoViewController *controller = segue.destinationViewController;
        UITableViewCell *cell = sender;
        // Get the index path for the selected cell
        NSIndexPath *index= [self.tableView indexPathForCell:cell];
        // Set photo and title of PhotoVC
        controller.photo = self.listOfPhotos[index.row];
        controller.title = cell.textLabel.text;
    }
}

#pragma mark -
#pragma mark Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Photo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [self.listOfPhotos[indexPath.row] valueForKey:SPOT_TITLE];
    cell.detailTextLabel.text = [self.listOfPhotos[indexPath.row] valueForKey:SPOT_DESCRIPTION];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.listOfPhotos count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // We call the delegate so chosen picture at the indexPath.row index can be added to the array with all the recent pictures.
    [self.delegate selectedPhoto:self.listOfPhotos[indexPath.row]];
}

@end
