//
//  SpotModel.h
//  Shutterbug
//
//  Created by manu on 07/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SPOT_TITLE @"spot_title"
#define SPOT_DESCRIPTION @"spot_description"
#define SPOT_ID @"spot_id"
#define SPOT_LARGE_PHOTO @"spot_large_photo"
#define SPOT_ORIGINAL_PHOTO @"spot_original_photo"

@interface SpotModel : NSObject
-(NSArray *)photosInCategory:(NSString *)category;
-(NSUInteger)numberPhotosInCategory:(NSString *)category;
-(NSArray *)photoCategories;
+(NSArray *)recentPhotos;
-(void)addToRecentPhotos:(NSDictionary *)photo;
@end
