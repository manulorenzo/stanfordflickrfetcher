//
//  CacheManager.h
//  Shutterbug
//
//  Created by manu on 22/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheManager : NSObject
+(void)addPhotoToCache:(NSDictionary *)photo withData:(NSData *)photoData;
+(BOOL)photoCached:(NSDictionary *)photo;
+(NSData *)photoFromCache:(NSDictionary *)photo;
@end
